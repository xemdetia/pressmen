-- Designed for Postgres 8.4.11 (Debian stable)

BEGIN;

CREATE TABLE pressmen.post_text (
       text_id SERIAL PRIMARY KEY,
       created timestamp DEFAULT CURRENT_TIMESTAMP,
       modified timestamp DEFAULT CURRENT_TIMESTAMP,
       content text NOT NULL );

CREATE TABLE pressmen.post_hdr (
       phdr_id SERIAL PRIMARY KEY, -- Serial is a specific keyword to
				   -- make an atomic sequence
       created timestamp DEFAULT CURRENT_TIMESTAMP,
       publish timestamp DEFAULT CURRENT_TIMESTAMP,
       text_id integer REFERENCES pressmen.post_text NOT NULL);

COMMIT;

CREATE FUNCTION pressmen.new_post_and_pub(content text) RETURNS integer AS $$

BEGIN
	INSERT INTO pressmen.post_text VALUES ( DEFAULT, DEFAULT, DEFAULT, content );
	INSERT INTO pressmen.post_hdr VALUES ( DEFAULT, DEFAULT, DEFAULT, lastval() );
	RETURN 0;
END;
$$ LANGUAGE plpgsql;


