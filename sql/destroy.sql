

BEGIN;

-- Drop Tables
DROP TABLE pressmen.post_hdr;
DROP TABLE pressmen.post_text;

-- Drop Functions
DROP FUNCTION IF EXISTS pressmen.new_post_and_pub(content text);
COMMIT;