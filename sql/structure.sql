-- Designed for Postgres 8.4.11 (Debian stable)

-- To do stored procedures, we need to make sure that the language is loaded.
CREATE LANGUAGE plpgsql;

-- Create a schema (database) that we are going to make our tables in:
CREATE SCHEMA pressmen;